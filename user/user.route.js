const express = require("express");
let router = express.Router();
let controller = require("./user.controller");
router.post("/", controller.post);
router.get("/", controller.get);
router.put("/", controller.put);
router.delete("/", controller.delete);

module.exports = router;

