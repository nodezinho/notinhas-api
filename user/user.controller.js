const User = require("./user.model");

module.exports.post = (req, res) => {
    if(!req.body) return res.send({
        success: false,
        message: "No body in request"
    });
    console.log(req.body);
    let user = new User({name: req.body.name, password: req.body.password});
    user.save((err, result) => {
        if (err){
            console.log(err.message);
            return res.send({
                success: false,
                message: err.message
            });
        }
        return res.send({
            success: true,
            message: "User created"
        });
    });
}

module.exports.get = (req, res) => {
    let criteria = {};
    if (req.query.id) criteria._id = req.query.id;
    let query = User
        .find(criteria)
        .select({"_id": 1, "name": 1});

    query.exec((err, users) => {
        if (err) {
            return res.send({
                success: false,
                message: err.message
            });
        }
        res.send({
            success: true,
            users: users
        });
    });
}

module.exports.put = (req, res) => {
    let update = {name: req.body.name, password: req.body.password};
    User.findOneAndUpdate(req.query.id, update, (err, user) => {
        if (err) {
            return res.send({
                success: false,
                message: err.message
            });
        }
        return res.send({
            success: true,
            message: user.name + " has updated"
        });
    })
}

module.exports.delete = (req, res) => {
    let criteria = {_id: req.query.id};
    User.deleteOne(criteria, (err) => {
        if (err){
            return res.send({
                success: false, 
                message: err.message
            });
        }
        return res.send({
            success: true,
            message: "User Removed"
        });
    });
}

