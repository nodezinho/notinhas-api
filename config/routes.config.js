module.exports = (api) => {
    api.use("/users", require("../user/user.route"));
    api.use("/notes", require("../note/note.route"));
}