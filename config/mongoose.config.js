module.exports = () => {
    const mongoose = require("mongoose");
    mongoose.connect("mongodb://drlawrence:asd123@ds147890.mlab.com:47890/notesdb");

    const db = mongoose.connection;
    db.on("error", (err) => {
        console.log("connection error:", err);
    })
    .once("open", () => {
        console.log("connection opened");
    });
}

