const morgan = require("morgan");
const bodyParser = require("body-parser");

module.exports = (api) => {
    api.use(morgan("dev"));
    api.use(bodyParser.urlencoded ({ extend : true}));
    api.use(bodyParser.json({ type: 'application/*+json' }))
    api.use( (req, res, next) => {
        res.setHeader('Access-Control-Allow-Origin', '*');
        res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
        res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Authorization, X-Access-Token, Origin, Accept');
        next();
    });

}