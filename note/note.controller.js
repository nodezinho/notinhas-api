const Note = require("./note.model");

module.exports.post = (req, res) => {
    if(!req.body) return res.send({
        success: false,
        message: "No body in request"
    });
    console.log(req.body);

    let note = new Note(req.body);

    note.save((err, result) => {
        if (err){
            console.log(err.message);
            return res.send({
                success: false,
                message: err.message
            });
        }
        return res.send({
            success: true,
            message: "Note created"
        });
    });
}

module.exports.get = (req, res) => {

    let criteria = req.query;
    console.log(criteria);

    let query = Note.find(criteria);

    query.exec((err, notes) => {
        if (err) {
            return res.send({
                success: false,
                message: err.message
            });
        }
        res.send({
            success: true,
            notes: notes
        });
    });
}

module.exports.put = (req, res) => {
    let note = req.body;
    note.updated = new Date();
    Note.findOneAndUpdate(req.query.id, note, (err, note) => {
        if (err) {
            return res.send({
                success: false,
                message: err.message
            });
        }
        return res.send({
            success: true,
            message: `note ${req.query.id} has been updated`
        });
    });
}

module.exports.delete = (req, res) => {
    let criteria = {_id: req.query.id};
    Note.deleteOne(criteria, (err) => {
        if (err){
            return res.send({
                success: false, 
                message: err.message
            });
        }
        return res.send({
            success: true,
            message: "note Removed"
        });
    });
}
