const express = require("express");
const controller = require("./note.controller");
let router = express.Router();

router.get("/", controller.get);
router.post("/", controller.post);
router.put("/", controller.put);
router.delete("/", controller.delete);

module.exports = router;