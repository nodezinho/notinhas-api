const mongoose = require("mongoose");

let Schema = mongoose.Schema({
    creator: {type: String, required: true},
    title: {type: String, required: true},
    body: {type: String},
    public: {type: Boolean, default: false, required: true},
    views: {type: Number, default: 0},
    created: {type: Date, default: Date.now},
    updated: {type: Date, default: Date.now}
});
module.exports = mongoose.model("Note", Schema);
