const express = require("express");
let api = express();
const port = process.env.PORT || 9000;

require("./config/express.config")(api);
require("./config/mongoose.config")(api);
require("./config/routes.config")(api)

api.listen(port, () => {
    console.log(`listening to: ${port}`);
});
